package Model;

public class Frame2 {
	private int size;
	
	public Frame2(int size)
	{
		this.size = size;
	}

	@Override
	public String toString() 
	{
		 String r = "";
		for (int i = 1 ; i <= size ; i++)
		{
			for (int j = 1 ; j <= i+size ; j++)
				r = r + "*";
			r = r + "\n";
		}
		return r ;
	}

}
