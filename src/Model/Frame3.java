package Model;

public class Frame3 {
	private int size;
	
	public Frame3(int size)
	{
		this.size = size;
	}
	
	@Override
	public String toString() 
	{
		 String r = "";
		for (int i = 1 ; i <= size ; i++)
		{
			for (int j = 1 ; j <= size ; j++)
			{
				if (j % 2 == 0)
					r = r + "*";
				else
					r = r + "-";
			}
				r = r + "\n";
		}
		return r ;
	}
}
