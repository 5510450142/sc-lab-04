package Model;

public class Frame4 {
	private int size;
	
	public Frame4(int size)
	{
		this.size = size;
	}
	
	@Override
	public String toString() 
	{
		 String r = "";
		for (int i = 1 ; i <= size ; i++)
		{
			for (int j = 1 ; j <= size ; j++)
			{
				if ((i + j) % 2 == 0)
					r = r + "*";
				else
					r = r + " ";
			}
				r = r + "\n";
		}
		return r ;
	}
}


