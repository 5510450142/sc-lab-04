package View;


import javax.swing.JFrame;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import Control.Control;


public class Gui extends JFrame {
	
	JPanel p1,p2;
	JFrame f1,f2;
	JComboBox<String> jcombo;
	JTextField size;
	JButton button;
	JLabel namesize,nameselect;
	Control c;
	JTextArea textArea;
	String result = "";
	public Gui(){
		c = new Control();
		p1 = new JPanel();
		p2 = new JPanel();
		textArea = new JTextArea();
		JScrollPane scroll = new JScrollPane (textArea);
		
		f1 = new JFrame();
		f2 = new JFrame();
		
		jcombo = new JComboBox<String>();
		jcombo.setBounds(100, 30, 250, 30);
		p1.add(jcombo);
		
		size = new JTextField();
		size.setBounds(100,90, 200, 25);
		p1.add(size); 
		
		button = new JButton("Total");
		button.setBounds(0, 127, 395, 40);
		p1.add(button);
		
		nameselect = new JLabel("Style : ");
		nameselect.setBounds(50, 20, 100, 50);
		p1.add(nameselect);
		
		namesize = new JLabel("Size :");
		namesize.setBounds(50, 75, 50, 50);
		p1.add(namesize);
		
		jcombo.addItem("Frame 1");
		jcombo.addItem("Frame 2");
		jcombo.addItem("Frame 3");
		jcombo.addItem("Frame 4");

		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				result+=c.outPut(jcombo.getSelectedIndex(),size.getText());
				textArea.setText(result);
				f2.add(p2);
				f2.setVisible(true);
				f2.setSize(400,400);
				f2.setLocation(50,50);
				f2.add(scroll);
			}
		});
		
		p1.setLayout(null);

		
		f1.add(p1);

		
		f1.setVisible(true);

		
		f1.setSize(400,200);
		f1.setLocation(500,200);
		f1.setResizable(false);
		

		
		
	}

}

